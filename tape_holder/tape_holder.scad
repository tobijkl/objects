W = 10;

A = 45; //Distance to wall.
B = 70; //Distance between rods.
D = 8; //Rod diameter.

E = 6 ; //Screw diameter.
F = 18; //Screw head diameter.
G = 20; //Screw length.

$fn = 100;

module t_slot(length=W) {
    translate([5,0,0]) cube([10, length, 20], center=true);
}

module rod(length=W) {
    rotate([90,0,0]) cylinder(d=D, h=length, center=true);
}

module thingi() {
    t_slot();
    translate([A,0,-30]) {
        rod();
        translate([B,0,0]) rod();
    }
}

difference() {
    minkowski() {
        hull() {
            thingi();
            translate([0,0,-30]) rod();
        }
        rotate([90,0,0]) cylinder(d=10,h=1, center=true);
    }
    scale([1,5,1]) thingi();
    translate([60,0,20]) rotate([90,0,0])
        hull() {
            cylinder(d=80,h=20, center=true);
            translate([100,0,0]) cylinder(d=80,h=20, center=true);
        }
    translate([-50,0,0]) cube([100,200,200], center=true);
    rotate([0,90,0]) cylinder(d=E, h=100);
    translate([G,0,0]) rotate([0,90,0]) cylinder(d=F, h=100);
    translate([0,-50,10]) cube([100,100,100]);
    
    translate([A,-7,-30]) {
        rotate([90,0,0])
        cylinder(h=15, d1=1, d2=2*D, center=true);
        translate([B,0,0]) rotate([90,0,0]) 
        cylinder(h=15, d1=1, d2=2*D, center=true);
    }
    
    translate([G,-15,0]) cube([30,30,30]);
}

//scale([1,10,1]) thingi();

module tape(D) {
    rotate([90,0,0]) cylinder(d=D, h=50, center=true);
}

//translate([80, 0, 25]) 
//color("green") tape(130);