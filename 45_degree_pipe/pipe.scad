$fn = 100;

OD = 55;
ID = OD-2*2;

R = 70;
E = 20;
A = 40;

WALL = 1.6;

module pipe(od, id, length) {
    difference() {
        rotate([0,90,0]) cylinder(d=od, h=length, center=true);
        rotate([0,90,0]) cylinder(d=id, h=length*2, center=true);
    }
}

translate([-E/2,-R,0]) 
pipe(OD+WALL, OD, E);

n = 100;
union()
for(i = [0 : 1 : n]) {
    a = OD+WALL;
    b = OD;

    a_i = OD-WALL;
    b_i = ID-WALL;

    
    rotate([0,0,i*A/n]) translate([0,-R,0]) 
    pipe(a-(a-b)*i/n, a_i-(a_i-b_i)*i/n, 1);
}

rotate([0,0,A]) translate([+E/2,-R,0]) 
pipe(ID, ID-WALL, E);