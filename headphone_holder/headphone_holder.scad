
NAME = "T";

W = 30;
H = 20;
L = 75;
HF = 35;
T = 5;
R = 25;


$fn = 50;

difference() { union() {
    hull() {
        translate([0,0,0]) intersection() {
            translate([0,0,-R+HF]) rotate([0,90,0]) cylinder(r=R, h=T);
            translate([0,-W/2,0]) cube([T, W, HF]);
        }

        translate([10,0,0]) intersection() {
            translate([0,0,-R+H]) rotate([0,90,0]) cylinder(r=R, h=T);
            translate([0,-W/2,0]) cube([T, W, H]);
        }
    }

    translate([0,0,0]) intersection() {
        translate([0,0,-R+2*T]) rotate([0,90,0]) cylinder(r=R, h=L);
        translate([0,-W/2,0]) cube([L, W, H]);
    }

    translate([L,0,0]) intersection() {
        translate([0,0,-R+H]) rotate([0,90,0]) cylinder(r=R, h=T);
        translate([0,-W/2,0]) cube([T, W, H]);
    }
    }
    translate([L+T-1,0,H/2]) rotate([90,0,90]) linear_extrude(height = 1)
    text(name, halign="center", valign="center");
     
    translate([8,0,HF-10]) {
        translate([+10,0,0]) rotate([0,90,0])
        cylinder(d=11, h=20, center=true);
        translate([-10,0,0]) rotate([0,90,0])
        cylinder(d=4, h=20, center=true);
    }   
}