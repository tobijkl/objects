D = 300;
d = 5.5;

h = 400;

$fn = 10;

N = 6;                    // >= 3
alpha = 360/N;            // angle
r = D/2;                  // outer radius
s = 2*r * sin(alpha/2);   // side length
a = r * cos(alpha/2);     // inner radius


//a = 2 * r_u * sin(36);
//r_i = (a/10) * sqrt(25+10*sqrt(5));

echo(a=a);

module sticks() {
    for(i = [0:N-1]) {
        rotate([0,0,i*alpha]) translate([a,0,0]) rotate([90,0,0])
        cylinder(d=d, h=s, center=true);
        rotate([0,0,i*alpha+alpha/2]) {
            translate([r, 0, 0]) sphere(d=d);
            translate([r, 0, -h]) rotate([0,0,0]) cylinder(d=d, h=h);
        }
    }
}


module edge() {
        difference() {
            minkowski() {
                hull() intersection() {
                    rotate([0,0,alpha/2]) translate([r, 0, 0])
                    cube([20,20,20], center=true);
                    sticks();
                }
                sphere(r=4);
            }
            sticks();
            rotate([0,0,alpha/2]) translate([r, 0, 20])
            cube([30, 30, 30], center=true);
        }
}


for (i = [0: N-1]) {
    rotate([0,0,i*alpha])
    edge();   
}
sticks();

// // single edge connector
// edge();
