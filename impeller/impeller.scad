$fn = 100;

difference() {
    union() {
        for ( i = [1:12]) { 
            rotate([0,0,i*360/12])
            translate([45/2-13,-1.5/2,0])
            cube([13, 1.5, 12.5]);
        }
        cylinder(d = 45, h=2);
        hull() {
            translate([0,0,7]) 
            cylinder(d = 6, h=1);
            cylinder(d = 8, h=1);
        }
    }
    cylinder(d=2.2, h=20, center=true);
}