W = 10;
T = 23.1;
D = 12.1;

$fn = 50;

module drawer() {
    translate([0,0,20]) intersection() {
        cube([W,T,50], center=true);
        translate([0,0,-10]) rotate([90-73,0,0])
        cube([W,2*T,50], center=true);
    }
}

module rod() {
    translate([0,20+20,0]) rotate([0,90,0])
    cylinder(d=D,h=W, center=true);
}


difference() {
    minkowski() {
        difference() {
            hull() {
                drawer();
                rod();
            }
            
            hull() {
                translate([0,33,25]) rotate([0,90,0])
                cylinder(d=40,h=2*W, center=true);
                translate([0,33,50]) rotate([0,90,0])
                cylinder(d=40,h=2*W, center=true);
                translate([0,50,25]) rotate([0,90,0])
                cylinder(d=40,h=2*W, center=true);
            }
        }
        //sphere(d=10);
        rotate([0,90,0]) cylinder(d=10);
    }
    scale([10,1,1]) {
        drawer();
        rod();
    }
    cube([W*10,T,50], center=true);
}
