// 
// roller blind connector
//

$fn = 100;

difference() {
    cube([7, 7, 20], center=true); 
    cylinder(d=2, h=H+2, center=true);
    cylinder(d=5, h=16, center=true);
    translate([5,0,0]) cube([10, 1.5, 22], center=true);
    rotate([0,90,0]) cylinder(d=4.5, h=16);
}